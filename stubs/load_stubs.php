<?php

if (!\class_exists('haoyundada\Exception')) {
    require __DIR__.'/Exception.php';
}

if (!\class_exists('haoyundada\Facade')) {
    require __DIR__.'/Facade.php';
}
