<?php

// +----------------------------------------------------------------------
// | HaoyundadaWordpress [ WE CAN DO IT JUST HAOYUNDADA ]
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind        : 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://zjzit.cn>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace haoyundada\db\exception;

class ModelNotFoundException extends DbException
{
    protected $model;

    /**
     * 构造方法.
     *
     * @param string $message
     * @param string $model
     * @param array  $config
     */
    public function __construct(string $message, string $model = '', array $config = [])
    {
        $this->message = $message;
        $this->model = $model;

        $this->setData('Database Config', $config);
    }

    /**
     * 获取模型类名.
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }
}
