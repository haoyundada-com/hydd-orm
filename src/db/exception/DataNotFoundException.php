<?php

// +----------------------------------------------------------------------
// | HaoyundadaWordpress [ WE CAN DO IT JUST HAOYUNDADA ]
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind        : 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://zjzit.cn>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace haoyundada\db\exception;

class DataNotFoundException extends DbException
{
    protected $table;

    /**
     * DbException constructor.
     *
     * @param string $message
     * @param string $table
     * @param array  $config
     */
    public function __construct(string $message, string $table = '', array $config = [])
    {
        $this->message = $message;
        $this->table = $table;

        $this->setData('Database Config', $config);
    }

    /**
     * 获取数据表名.
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }
}
