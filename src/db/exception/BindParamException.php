<?php

// +----------------------------------------------------------------------
// | HaoyundadaWordpress [ WE CAN DO IT JUST HAOYUNDADA ]
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind        : 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://zjzit.cn>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace haoyundada\db\exception;

/**
 * PDO参数绑定异常.
 */
class BindParamException extends DbException
{
    /**
     * BindParamException constructor.
     *
     * @param string $message
     * @param array  $config
     * @param string $sql
     * @param array  $bind
     * @param int    $code
     */
    public function __construct(string $message, array $config, string $sql, array $bind, int $code = 10502)
    {
        $this->setData('Bind Param', $bind);
        parent::__construct($message, $config, $sql, $code);
    }
}
